package org.example.appsoliqtask.exceptions;


public class SystemException extends RuntimeException {

    public SystemException(String message) {
        super(message);
    }
}
