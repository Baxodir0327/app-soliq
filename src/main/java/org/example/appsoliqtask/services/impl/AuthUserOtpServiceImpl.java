package org.example.appsoliqtask.services.impl;

import lombok.RequiredArgsConstructor;
import org.example.appsoliqtask.entities.AuthUser;
import org.example.appsoliqtask.entities.AuthUserOtp;
import org.example.appsoliqtask.repositories.AuthUserOtpRepository;
import org.example.appsoliqtask.services.AuthUserOtpService;
import org.example.appsoliqtask.utils.BaseUtils;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;


import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class AuthUserOtpServiceImpl implements AuthUserOtpService {
    private final AuthUserOtpRepository repository;
    private final BaseUtils utils;

    @Override
    public AuthUserOtp create(@NonNull AuthUserOtp authUserOtp) {
        return repository.save(authUserOtp);
    }

    @Override
    public AuthUserOtp createOTP(@NonNull AuthUser authUser) {
        AuthUserOtp authUserOtp = AuthUserOtp.builder()
                .code(utils.generateOtp(authUser.getId()))
                .userID(authUser.getId())
                .expiresAt(LocalDateTime.now().plusMinutes(10))
                .build();
        return create(authUserOtp);
    }
}
