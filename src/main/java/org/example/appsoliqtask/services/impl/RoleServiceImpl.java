package org.example.appsoliqtask.services.impl;

import org.example.appsoliqtask.dtos.request.CreateRoleRequest;
import org.example.appsoliqtask.dtos.response.CreatRoleResponse;
import org.example.appsoliqtask.dtos.response.RoleDto;
import org.example.appsoliqtask.entities.Role;
import org.example.appsoliqtask.enums.Authority;
import org.example.appsoliqtask.exceptions.SystemException;
import org.example.appsoliqtask.repositories.RoleRepository;
import org.example.appsoliqtask.services.RoleService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepository repository;

    public RoleServiceImpl(RoleRepository repository) {
        this.repository = repository;
    }

    @Override
    public CreatRoleResponse create(CreateRoleRequest request) {
        if (request.getName() == null || request.getName().isEmpty() || request.getName().contains(" "))
            throw new SystemException("Invalid role name!");

        if (repository.existsRoleByName(request.getName()))
            throw new SystemException("This name has already been used!");

        Role role = new Role();
        role.setName(request.getName());
        role.setAuthorities(new HashSet<>());

        repository.save(role);

        CreatRoleResponse response = new CreatRoleResponse();
        response.setId(role.getId());
        response.setName(role.getName());
        response.setAuthorities(role.getAuthorities());

        return response;
    }

    @Override
    public List<RoleDto> getAll() {
        return repository.findAll().stream()
                .map(this::fromEntityToDto).
                collect(Collectors.toList());
    }

    @Override
    public RoleDto getById(Long id) {
        Optional<Role> byId = repository.findById(id);

        if (byId.isEmpty())
            throw new SystemException("Role with id %d does not exist!".formatted(id));

        return fromEntityToDto(byId.get());
    }

    @Override
    public RoleDto addAuthority(Long id, String name) {
        Optional<Role> roleById = repository.findById(id);

        if (roleById.isEmpty())
            throw new SystemException("Role with id % d does not exist !".formatted(id));

        try {
            Authority authority = Authority.valueOf(name);
            Set<Authority> authorities = roleById.get().getAuthorities();

            if (authorities == null) {
                authorities = new HashSet<>();
                roleById.get().setAuthorities(authorities);
            }

            if (authorities.contains(authority))
                throw new SystemException("This authority has already exists!");

            authorities.add(authority);
            repository.save(roleById.get());
            return fromEntityToDto(roleById.get());

        } catch (Exception e) {
            throw new SystemException("Invalid authority name!");
        }
    }

    @Override
    public RoleDto removeAuthority(Long id, String name) {
        Optional<Role> roleById = repository.findById(id);

        if (roleById.isEmpty())
            throw new SystemException("Role with id % d does not exist !".formatted(id));

        try {
            Authority authority = Authority.valueOf(name);
            Set<Authority> authorities = roleById.get().getAuthorities();

            if (authorities == null) {
                authorities = new HashSet<>();
                roleById.get().setAuthorities(authorities);
            }

            if (!authorities.contains(authority))
                throw new SystemException("This role does not have this authority!");

            authorities.remove(authority);
            repository.save(roleById.get());
            return fromEntityToDto(roleById.get());

        } catch (Exception e) {
            throw new SystemException("Invalid authority name!");
        }
    }

    @Override
    public boolean delete(Long id) {
        Optional<Role> byId = repository.findById(id);
        if (byId.isEmpty())
            throw new SystemException("Roles does not exist !");

        repository.delete(byId.get());
        return true;
    }

    @Override
    public RoleDto fromEntityToDto(Role role) {
        RoleDto dto = new RoleDto();
        dto.setId(role.getId());
        dto.setName(role.getName());
        dto.setAuthorities(role.getAuthorities());
        return dto;
    }
}
