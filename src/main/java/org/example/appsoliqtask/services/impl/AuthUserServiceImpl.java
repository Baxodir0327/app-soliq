package org.example.appsoliqtask.services.impl;

import lombok.RequiredArgsConstructor;
import org.example.appsoliqtask.dtos.AuthUserCreateDto;
import org.example.appsoliqtask.dtos.request.GenerateTokenRequest;
import org.example.appsoliqtask.dtos.response.UserDto;
import org.example.appsoliqtask.entities.AuthUser;
import org.example.appsoliqtask.entities.AuthUserOtp;
import org.example.appsoliqtask.entities.Role;
import org.example.appsoliqtask.exceptions.SystemException;
import org.example.appsoliqtask.mappers.AuthUserMapper;
import org.example.appsoliqtask.repositories.AuthUserOtpRepository;
import org.example.appsoliqtask.repositories.AuthUserRepository;
import org.example.appsoliqtask.repositories.RoleRepository;
import org.example.appsoliqtask.security.JwtTokenUtil;
import org.example.appsoliqtask.services.AuthUserOtpService;
import org.example.appsoliqtask.services.AuthUserService;
import org.example.appsoliqtask.services.RoleService;
import org.example.appsoliqtask.utils.MailSenderService;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuthUserServiceImpl implements AuthUserService {
    private final AuthUserMapper authUserMapper;
    private final AuthUserRepository authUserRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final MailSenderService mailSenderService;
    private final AuthUserOtpService authUserOtpService;
    private final AuthUserOtpRepository authUserOtpRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleService roleService;
    private final RoleRepository roleRepository;

    @Override
    public String register(@NonNull AuthUserCreateDto dto) {
        AuthUser authUser = authUserMapper.toEntity(dto);
        if (authUserRepository.findByEmail(dto.getEmail()).isPresent())
            throw new RuntimeException("Email Already Taken");

        if (authUserRepository.findByUsername(dto.getUsername()).isPresent())
            throw new RuntimeException("Username Already Taken");

        authUser.setPassword(passwordEncoder.encode(authUser.getPassword()));
        authUserRepository.save(authUser);
        AuthUserOtp authUserOtp = authUserOtpService.createOTP(authUser);
        Map<String, String> model = new HashMap<>();
        model.put("to", authUser.getEmail());
        model.put("code", authUserOtp.getCode());
        mailSenderService.sendActivationMail(model);
        /*
        * shu joyda mailtrap.id testviy gmailga xabar boradi buni fremekir cutubxonasi yordamida html page yuborganman
        * */
        return "Success";
    }

    @Override
    public String generateToken(@NonNull GenerateTokenRequest request) {
        String username = request.getUsername();
        String password = request.getPassword();
        var authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        authenticationManager.authenticate(authenticationToken);
        return jwtTokenUtil.generateToken(username);
    }

    @Override
    public String activateAccount(@NonNull String code) {
        System.out.println(code);
        AuthUserOtp otp = authUserOtpRepository.findByCodeIgnoreCase(code)
                .orElseThrow(() -> new RuntimeException("Invalid Code"));

        if (otp.getExpiresAt().isBefore(LocalDateTime.now()))
            throw new RuntimeException("Code is expired");

        Long userID = otp.getUserID();
        authUserRepository.activateUser(userID);
        return "Account Successfully Activated";
    }

    @Override
    public AuthUser findByUsername(String username) {

        Optional<AuthUser> byUsername = authUserRepository.findByUsername(username);
        return byUsername.get();
    }

    @Override
    public UserDto getUser(Long id) {
        Optional<AuthUser> byId = authUserRepository.findById(id);

        if (byId.isEmpty())
            throw new SystemException("User not found!");

        return fromEntityToDto(byId.get());
    }

    @Override
    public UserDto addRole(Long id, Long roleID) {
        Optional<AuthUser> byId = authUserRepository.findById(id);
        Optional<Role> roleById = roleRepository.findById(roleID);

        if (byId.isEmpty())
            throw new SystemException("User not found!");

        if (roleById.isEmpty())
            throw new SystemException("Role  not found!");

        byId.get().getRoles().add(roleById.get());

        authUserRepository.save(byId.get());

        return fromEntityToDto(byId.get());
    }

    @Override
    public UserDto removeRole(Long id, Long roleId) {
        Optional<AuthUser> byId = authUserRepository.findById(id);
        Optional<Role> roleById = roleRepository.findById(roleId);

        if (byId.isEmpty())
            throw new SystemException("User not found!");

        if (roleById.isEmpty())
            throw new SystemException("Role  not found!");

        byId.get().getRoles().remove(roleById.get());

        authUserRepository.save(byId.get());

        return fromEntityToDto(byId.get());
    }

    UserDto fromEntityToDto(AuthUser user) {
        UserDto dto = new UserDto();
        dto.setId(user.getId());
        dto.setUsername(user.getUsername());
        dto.setRoles(user.getRoles().stream()
                .map(roleService::fromEntityToDto)
                .collect(Collectors.toSet()));
        dto.setEmail(user.getEmail());
        dto.setActive(user.isActive());
        return dto;
    }
}
