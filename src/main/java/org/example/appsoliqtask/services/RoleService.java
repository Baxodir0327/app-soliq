package org.example.appsoliqtask.services;

import org.example.appsoliqtask.dtos.request.CreateRoleRequest;
import org.example.appsoliqtask.dtos.response.CreatRoleResponse;
import org.example.appsoliqtask.dtos.response.RoleDto;
import org.example.appsoliqtask.entities.Role;

import java.util.List;

public interface RoleService {

    CreatRoleResponse create(CreateRoleRequest request);

    List<RoleDto> getAll();

    RoleDto getById(Long id);

    RoleDto addAuthority(Long id, String name);

    RoleDto removeAuthority(Long id, String name);

    boolean delete(Long id);

    RoleDto fromEntityToDto(Role role);
}
