package org.example.appsoliqtask.services;

import org.example.appsoliqtask.dtos.AuthUserCreateDto;
import org.example.appsoliqtask.dtos.request.GenerateTokenRequest;
import org.example.appsoliqtask.dtos.response.UserDto;
import org.example.appsoliqtask.entities.AuthUser;
import org.springframework.lang.NonNull;


public interface AuthUserService {
    String register(@NonNull AuthUserCreateDto dto);

    String generateToken(@NonNull GenerateTokenRequest request);

    String activateAccount(@NonNull String code);

    AuthUser findByUsername(String username);
    UserDto getUser(Long id);
    UserDto addRole(Long id, Long roleId);
    UserDto removeRole(Long id, Long roleId);
}
