package org.example.appsoliqtask.services;

import org.example.appsoliqtask.entities.AuthUser;
import org.example.appsoliqtask.entities.AuthUserOtp;
import org.springframework.lang.NonNull;


public interface AuthUserOtpService {

    AuthUserOtp create(@NonNull AuthUserOtp authUserOtp);

    AuthUserOtp createOTP(@NonNull AuthUser authUser);
}
