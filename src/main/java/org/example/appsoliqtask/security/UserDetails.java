package org.example.appsoliqtask.security;


import org.example.appsoliqtask.entities.AuthUser;
import org.example.appsoliqtask.enums.Authority;
import org.springframework.lang.NonNull;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;

public class UserDetails implements org.springframework.security.core.userdetails.UserDetails {

    private Long id;
    private final AuthUser authUser;
    private String email;

    public UserDetails(@NonNull AuthUser authUser) {
        this.authUser = authUser;
        this.id = authUser.getId();
        this.email = authUser.getEmail();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<Authority> authorities = authUser.getRoles()
                .stream()
                .flatMap(it ->
                        it.getAuthorities().stream())
                .toList();

        return authorities.stream()
                .map(it -> (GrantedAuthority) it::name)
                .toList();
    }

    @Override
    public String getPassword() {
        return authUser.getPassword();
    }

    @Override
    public String getUsername() {
        return authUser.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return authUser.isActive();
    }

    @Override
    public boolean isAccountNonLocked() {
        return authUser.isActive();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return authUser.isActive();
    }

    @Override
    public boolean isEnabled() {
        return authUser.isActive();
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }
}
