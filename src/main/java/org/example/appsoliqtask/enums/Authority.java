package org.example.appsoliqtask.enums;

public enum Authority {
    SUPER_AUTH,
    CREATE_ROLE,
    GET_ALL_ROLES,
    GET_ROLE_BY_ID,
    ADD_AUTHORITY_TO_ROLE,
    REMOVE_AUTHORITY_TO_ROLE,
    DELETE_ROLE,
    GET_USER_INFO,
    ADD_ROLE_FROM_USER,
    REMOVE_ROLE_FROM_USER
}
