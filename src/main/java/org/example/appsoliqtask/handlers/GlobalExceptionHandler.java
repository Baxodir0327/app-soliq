package org.example.appsoliqtask.handlers;


import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.example.appsoliqtask.dtos.response.AppErrorDto;
import org.example.appsoliqtask.dtos.response.ErrorResponse;
import org.example.appsoliqtask.exceptions.SystemException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.*;


@RestControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {
    private final ObjectMapper objectMapper;

    @ExceptionHandler(SystemException.class)
    public ResponseEntity<?> handleSystemException(SystemException exception) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setCode(413);
        errorResponse.setMessage(exception.getMessage());
        return ResponseEntity.status(413).body(errorResponse);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<AppErrorDto> handleMethodArgumentNotValidException(MethodArgumentNotValidException e, HttpServletRequest request)  {
        String friendlyMessage = "Not Valid Input";
        String errorPath = request.getRequestURI();
        Map<String, List<String>> developerMessage = new HashMap<>();
        for (FieldError fieldError : e.getFieldErrors()) {
            String field = fieldError.getField();
            String message = fieldError.getDefaultMessage();
            developerMessage.compute(field, (s, values) -> {
                if (!Objects.isNull(values))
                    values.add(message);
                else
                    values = new ArrayList<>(Collections.singleton(message));
                return values;
            });
        }
        AppErrorDto appErrorDto = new AppErrorDto(friendlyMessage, developerMessage, errorPath, 400);
        return ResponseEntity.status(400).body(appErrorDto);
    }


    @ExceptionHandler()
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<AppErrorDto> handleRuntimeException(RuntimeException e, HttpServletRequest request) {
        String friendlyMessage = "Internal Server Error";
        String developerMessage = e.getMessage();
        String errorPath = request.getRequestURI();
        AppErrorDto appErrorDto = new AppErrorDto(friendlyMessage, developerMessage, errorPath, 500);
        return ResponseEntity
                .status(500)
                .body(appErrorDto);
    }

}
