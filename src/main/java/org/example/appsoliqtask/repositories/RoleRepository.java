package org.example.appsoliqtask.repositories;

import org.example.appsoliqtask.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    boolean existsRoleByName(String name);
    boolean existsRoleById(Long id);
}
