package org.example.appsoliqtask.entities;

import jakarta.persistence.*;
import lombok.*;
import org.example.appsoliqtask.enums.Authority;

import java.util.Set;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity(name = "roles")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @ElementCollection(targetClass = Authority.class, fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "role_authories", joinColumns = @JoinColumn(name = "role_id"))
    @Column(name = "authority_value")
    private Set<Authority> authorities;

}
