package org.example.appsoliqtask.mappers;

import org.example.appsoliqtask.dtos.AuthUserCreateDto;
import org.example.appsoliqtask.entities.AuthUser;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;



@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public interface AuthUserMapper {
    AuthUser toEntity(AuthUserCreateDto authUserCreateDto);
}