package org.example.appsoliqtask;

import org.example.appsoliqtask.security.SessionUser;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Optional;

@SpringBootApplication
@EnableJpaAuditing
@EnableAsync
@EnableScheduling
public class AppSoliqTask {


    public static void main(String[] args) {
        SpringApplication.run(AppSoliqTask.class, args);
    }

    @Bean
    CommandLineRunner runner() {
        return (args -> {
        });
    }

    @Bean
    public AuditorAware<Long> getAuditor(SessionUser sessionUser) {
        return () -> Optional.of(sessionUser.id());
    }


}
