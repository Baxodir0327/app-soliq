package org.example.appsoliqtask.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/test")
@RequiredArgsConstructor
public class TestController {

    @PostMapping("/test1")
    @PreAuthorize("hasAuthority('TEST_AUTHORITY')")
    public ResponseEntity<String> test() {
        return ResponseEntity.ok("Ok");
    }

    @PostMapping("/check_debit")
    @PreAuthorize("hasAuthority('CHECK_AUTHORITY')")
    public ResponseEntity<?> checkAuthority() {
        return ResponseEntity.ok("CHECKING SUCCESS !");
    }
}