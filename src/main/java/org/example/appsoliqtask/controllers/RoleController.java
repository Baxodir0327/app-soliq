package org.example.appsoliqtask.controllers;

import org.example.appsoliqtask.dtos.request.CreateRoleRequest;
import org.example.appsoliqtask.services.RoleService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/role")
public class RoleController {

    private final RoleService service;

    public RoleController(RoleService service) {
        this.service = service;
    }


    @PostMapping("/create")
    @PreAuthorize("hasAnyAuthority('CREATE_ROLE', 'SUPER_AUTH')")
    public ResponseEntity<?> create(@RequestBody CreateRoleRequest request) {
        return ResponseEntity.ok(service.create(request));
    }

    @GetMapping("/all")
    @PreAuthorize("hasAnyAuthority('GET_ALL_ROLES', 'SUPER_AUTH')")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(service.getAll());
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('GET_ROLE_BY_ID', 'SUPER_AUTH')")
    public ResponseEntity<?> findById(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(service.getById(id));
    }

    @PostMapping("/add-authority")
    @PreAuthorize("hasAnyAuthority('ADD_AUTHORITY_TO_ROLE', 'SUPER_AUTH')")
    public ResponseEntity<?> addAuthority(@RequestParam(name = "id") Long id,
                                          @RequestParam(name = "name") String name) {
        return ResponseEntity.ok(service.addAuthority(id, name));
    }

    @PostMapping("/remove-authority")
    @PreAuthorize("hasAnyAuthority('REMOVE_AUTHORITY_TO_ROLE', 'SUPER_AUTH')")
    public ResponseEntity<?> removeAuthority(@RequestParam(name = "id") Long id,
                                             @RequestParam(name = "name") String name) {
        return ResponseEntity.ok(service.removeAuthority(id, name));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('DELETE_ROLE', 'SUPER_AUTH')")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        service.delete(id);
        return ResponseEntity.ok("Deleted successfully");
    }
}
