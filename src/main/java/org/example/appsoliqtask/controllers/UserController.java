package org.example.appsoliqtask.controllers;

import org.example.appsoliqtask.services.AuthUserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
public class UserController {
    private final AuthUserService service;

    public UserController(AuthUserService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('GET_USER_INFO', 'SUPER_AUTH')")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(service.getUser(id));
    }

    @PostMapping("/add-role")
//    @PreAuthorize("hasAnyAuthority('ADD_ROLE_FROM_USER', 'SUPER_AUTH')")
    public ResponseEntity<?> addRole(@RequestParam("user-id") Long userId,
                                     @RequestParam("role-id") Long roleId){
        return ResponseEntity.ok(service.addRole(userId, roleId));
    }

    @PostMapping("/remove-role")
    @PreAuthorize("hasAnyAuthority('REMOVE_ROLE_FROM_USER', 'SUPER_AUTH')")
    public ResponseEntity<?> removeRole(@RequestParam("user-id") Long userId,
                                     @RequestParam("role-id") Long roleId){
        return ResponseEntity.ok(service.removeRole(userId, roleId));
    }
}
