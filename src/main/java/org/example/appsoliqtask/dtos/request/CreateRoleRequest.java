package org.example.appsoliqtask.dtos.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class CreateRoleRequest {
    @JsonProperty("name")
    private String name;
}
