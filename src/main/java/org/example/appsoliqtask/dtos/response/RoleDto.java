package org.example.appsoliqtask.dtos.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.example.appsoliqtask.enums.Authority;

import java.util.Set;

@Data
@RequiredArgsConstructor
public class RoleDto {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("authorities")
    private Set<Authority> authorities;
}
