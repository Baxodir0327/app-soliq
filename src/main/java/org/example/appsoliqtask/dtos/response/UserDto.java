package org.example.appsoliqtask.dtos.response;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.Set;

@Data
@RequiredArgsConstructor
public class UserDto  {

    private Long id;

    private String username;

    private String email;

    private Set<RoleDto> roles;

    private boolean active;
}
